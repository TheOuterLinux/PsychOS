################################################################################
# PsychOS 09/20/2016 - PsychOS 2.5.7 - cdw, alpine, and copying power settings #
################################################################################

* cdw is missing icon in menu

* Look into copying power settings in /home and make sure to turn fading off
    - Turning fading off should stop the issue of showing the desktop for a
      split second when waking the computer from suspension before
      asking for a password.
      
* alpine mail application is in wrong category
    - May make a terminal application category
