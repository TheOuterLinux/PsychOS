#####################################################################################
# 2019/01/09 - PsychOS 3.0.8 - Freeing up space, Raven clone idea, and adding glade #
#####################################################################################

I may be able to make more room if needed by removing /usr/share/doc files.

I just had an interesting idea; would it be possible to make a fake Raven,
as in Raven from Budgie desktop, using GAMBAS and make it visible only on
mouse-over? I would still need to figure out how to make it available on
all workspaces and hover above everything.

Add a set as GRUB image item to Thunar's custom actions

!![2019/02/15] Not going to happen. Using classic look instead; too risky.

Add 'glade' package.
