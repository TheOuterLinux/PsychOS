#######################################################################
# 2019/06/19 - PsychOS 3.4.6 - Added BAM to Thunar Custom Actions and #
#                              edited /etc/issue                      #
#######################################################################

I added a "Play BAM file" item to Thunar Custom Actions, but it turns out
that DeaDBeeF already plays it... The issue of not having it open with
DeaDBeeF by default is because doing so would enable all other unknown
file-types to use it as the default.

Edited the "/etc/issue" file to better reflect PsychOS. This is the file
that controls the text shown before logging in when in TTY/console.
