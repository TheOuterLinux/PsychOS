########################################################
# 2018/12/30 - PsychOS 3.0.0 - Adding installer issues #
########################################################

During the install of Devuan (Live not an option), I got a
missing firmware files message:
       b43/ucode11.fw
       b43/ucode11.fw
       b43-open/ucode11.fw
       b43-open/ucode11.fw
            
It seems as though these are for WiFi, but are not included 
with Devuan. 

During the install, I have the opportunity to insert another 
apt-based Linux distro to add more repos or packages, I think. 
My laptop wouldn't let me remove the DVD. Doing this via USB 
may have allowed this, but using a USB/DVD player didn't help.
         
Devuan uses 'popularity-contest.'
Run 'dpkg-reconfigure popularity-contest' and make sure to 
remove it.       
 
Added the libre-kernel (4.19.x) and it's too slow. I blame
Intel, trying to kill two birds with one stone by both
appearing to fix security issues (Meltdown & Spectre) and to
make older hardware slow, forcing people to buy new hardware,
which will more than likely contain their microchips. You would
think that being the libre-kernel that it wouldn't matter, but
it does.
           
I added 'debian-installer' package and also 'libdebian-installer-extra4'
but nothing showed-up on the menu. I added 'simple-ccd' with
'-no-install-recommends,' but nothing either.
           
Need to make sure to install 'software-properties-common' and
'dirmngr' so I can add repositories.
           
For Respin to install, I had to:
       su
       apt install memtest86+ squashfs-tools live-boot
       apt -fix-broken install
               
       **If you notice the 'su' instead of 'sudo,' this is because
         Devuan by default doesn't add users to the sudo group.
         This can now be fixed during the install.
                 

I'm running 'sudo respin dist' this time. Hopefully, it will
include its own installer because AntiX 17 might have failed
due to creating a Live snapshot and using its installer.
