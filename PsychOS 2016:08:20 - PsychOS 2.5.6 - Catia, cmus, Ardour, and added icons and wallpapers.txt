############################################################################################
# PsychOS 08/20/2016 - PsychOS 2.5.6 - Catia, cmus, Ardour, and added icons and wallpapers #
############################################################################################

* Installed Catia through Cadence via source code
    - Catia is a much more intuitive version of qjackctl
    - Dependencies and recommends:
        [] libjack-devel
        [] libqt4-devel
        [] python-qt4-devel
        [] python3-qt4-devel
        [] a2jmidid
        [] pulseaudio-module-jack
        
* Installed cmus -> a console based music player

* Added icons for menu items

* Added wallpapers to GNOME

* Tried to install Ardour from source code but failed at dependency fftwf3
    - Packages installed:
        [] boost-devel
        [] alsa-devel
        [] libsndfile-devel
        [] liblo-devel
        [] Installed taglib from source
        [] Installed vamp-plugin-sdk from source
        [] librubberband-devel
        [] Installed fftw3f from source
        
* ./waf configure in Ardour source directory failed here

* Developer package found via another repo, but susestudio repository limit
  has already been reached
    - Went ahead and added the above packages for Ardour to PsychOS
      but found a nightly build in the form of a .run file that takes care
      of everything automatically. Hopefully, susestudio will have no
      problem with it via the script run.
      
* Started to look into clipboard managers for storing different types of 
  items that can be copied and pasted. Turns out that XFCE already has 
  this feature via Clipman that can be found in the panel as a plugin. 
  It'll keep items regardless of closing an application and can be cleared 
  anytime.
