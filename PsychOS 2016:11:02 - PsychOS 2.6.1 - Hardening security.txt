###################################################
# 2016/11/02 - PsychOS 2.6.1 - Hardening security #
###################################################

*Fixed version number

*Add /etc/ssh/ssh_config and sshd_config files to the overlay

*Add rkhunter_config file to overlay

*Check to see how to update Lynis
	*[Update 2017/03/03] - Turns out Lynis has a repository; wasn't added to PsychOS 2.6.1

*Add /etc/php5/cli/php.ini file to overlay

*Look into installing Linux Malware Detect (LMD)
