#####################################################
# 2017/03/23 - PsychOS 2.7.0 - Sensors and rangebox #
#####################################################

*Add geopip

*Add power and temperature tools to system or settings depending if it just monitors or can change things
	*powertop to both
	*sensors for temperature; add to system
		-by default, sensors use Celsius; I may be able to change this as a preface setting for climenu

*Trying to use a rangebox for burning images, but it only uses whole numbers. I guess I could do 80/10 to return 
	integers.
		*Never mind; you can't really have #.n pixels anyway.
