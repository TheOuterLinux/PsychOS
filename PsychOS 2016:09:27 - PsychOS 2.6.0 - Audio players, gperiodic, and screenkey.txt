################################################################################
# PsychOS 09/27/2016 - PsychOS 2.6.0 - Audio players, gperiodic, and screenkey #
################################################################################

* mpg123 -C -@url.m3u can be used to play radio streams. Also, the -C is a
flag to allow controls. Typing "h" shows control options. Typing "l" will show a
playlist with a URL. That URL can be used with wget to download whatever is
playing in the stream in real time. The download can be stopped any time.

* Install faad2

* Install deadbeef
     - deadbeef-plugin-statusnotifier
     - It's a very simple but still feature rich player; could be used to
       separate music (Clementine) from sound files (DeaDBeef).
       
* Install gperiodic for a periodic table reference

* Installed screenkey to display keyboard presses if one wanted to make a tutorial

* Removed power manager .xml file.
