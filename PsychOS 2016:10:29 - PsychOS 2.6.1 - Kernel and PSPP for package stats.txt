##################################################################
# 2016/10/29 - PsychOS 2.6.1 - Kernel and PSPP for package stats #
##################################################################

*Updated the kernel and it caused PsychOS on my Macbook to not work. So, I'm reinstalling, but I'm adding the kernel repo and updating before installing to see if that'll work.
	*[Update 2017/03/03] - I think the wrong repository was used.

*Need to change version number

*http://download.opensuse.org/repositories/Kernel:/stable/standard/Kernel:stable.repo

*You can install a different kernel before install PsychOS, but it uses a lot of RAM and it broke PsychOS. I may try the PAE version of default kernel though.

*Finally got a CUPS Printer Test Page to print from PsychOS

*Going to try to get the PAE version of the kernel I need installed.
	*http://download.opensuse.org/repositories/Kernel:/openSUSE-13.2/standard/Kernel:openSUSE-13.2.repo
	*Then do a zypper se for "PAE" and install kernel-pae
	*Update

*Installing the PAE version of the default PsychOS kernel works

*Used zypper search -s | grep "i|" > list.txt to list all installed packages and then have "|" used as a seperator to import into a spreadsheet. PSPP was then able to open the .ods file with no problem. This is important because it lets me see what percentages regarding repositories make up PsychOS and see if some can be eliminated.

*Got rid of paper-icon-theme and the home:snwh:paper_openSUSE_13.2 repository and replace with the Kernel repo for PAE people.
