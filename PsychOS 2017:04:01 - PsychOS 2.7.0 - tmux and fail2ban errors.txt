#########################################################
# 2017/04/01 - PsychOS 2.7.0 - tmux and fail2ban errors #
#########################################################

*USB mounting

*HTML to PDF and then PDF viewing isn't working; tmux not TTY enough
	*I think there might be something wrong with fbiterm
		*[Update 2017/04/05] - tmux is running a subshell and therefore not TTY to use 
			frame buffering

*Change $HOME to $HOME/Documents in dialog file selector in ./office-list

*!!! fail2ban failed; one of the Python from sources failed; some packages installed missing 
	dependencies.
