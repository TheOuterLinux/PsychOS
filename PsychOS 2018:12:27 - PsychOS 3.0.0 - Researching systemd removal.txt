############################################################
# 2018/12/27 - PsychOS 3.0.0 - Researching systemd removal #
############################################################

*Replacing systemd with sysvinit
    apt-get install sysvinit systemd- [post from 2014]
    
*According to apt-cache rdepends -installed systemd:
    udev
    ifupdown
    libpam-systemd
    libnss-myhostname
    xserver-xorg-core
    ... and a few others
    
*You can also run "aptitude why -v systemd" or "sudo apt -s remove systemd" to simulate systemd removal to see what would happen.

*After running a simulated removal of systemd on MX Linux 17, these depend on it:
    bleachbit
    colord
    grub-customizer
    gufw
    gvfs
    gvfs-backends
    gvfs-daemons
    gvfs-fuse
    k3b
    libpam-systemd
    live-usb-maker-gui
    networkmanager-*
    policykit-1-*
    synaptic
    systemd
    udisk2
