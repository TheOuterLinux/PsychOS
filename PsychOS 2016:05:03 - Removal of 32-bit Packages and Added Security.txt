####################################################################
# PsychOS 05/03/2016 - Removal of 32-bit Packages and Added Security #
####################################################################

* 32-bit packages need to be removed from the 64-bit version of PsychOS.
  Afterwards, the 64-bit version of PsychOS needs to be tested.

* The 32-bit version of PsychOS EULA was updated to reflect the addition 
  of Lynis (Rkhunter fork), Java, and Canon drivers.
  
* Peerguardian was also added for extra security.

* I wanted to add Tor-Browser, but that may not be the wisest choice due 
  to potential tampering of RPM packages.

* If successful, upload both copies to google drive.
