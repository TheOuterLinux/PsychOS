########################################################################
# 2019/01/12 - PsychOS 3.0.9 - Menu editing, Bluetooth Blacklisting,   #
#                              and idea of adding FreeDOS ISO to GRUB  #
########################################################################

To edit the XFCE menu, install menulibre. The out-of-the-box menu categories
are in "/usr/share/desktop-directories/".
Make sure ".desktop" files contain "Categories=Terminal;XFCE;"

I cannot add Graphics category to 'pxltrm' without it adding itself to the
Graphics section of the XFCE menu. To solve this problem for a possible
'CLIMax' menu entry, I could have it 'grep' for "Categories=Terminal" and
"Keywords=Graphics".

    !![2019/02/15] - Tying-up these notes, I'm just as confused as to what
                     I meant in the above as you probably are. Of course
                     adding "Graphics" would put it in the "Graphics"
                     Category. Maybe I meant "Other?"
                     
'toxic' is broke and I have no idea why

I'll have to make the 'isight-firmware-tools' as an installer.

I think I'll disable BT by default. "/etc/modprobe.d/blacklist.conf"
    - blacklist bnep
    - blacklist btusb
    - blacklist bluetooth
I should be ablt to enable it back again via 'sudo modprobe bnep' and etc.

    !![2019/02/15] - This "feature" was created later but only 'btusb' was
                     blacklisted.
                     
I think I just had a really cool idea. Why not get a ISO copy of FreeDOS and
add it to the GRUB menu? I may even be able to use the opportunity segue into
a "PsychDOS."
