###################################################################
# 2019/01/08 - PsychOS 3.0.6 - Inkscape defaults, large packages, #
#                              and successful ISO install         #
###################################################################

For custom Inkscape default document, look inside of /usr/share/inkscape/templates
and edit default.svg's File -> Document Properties.

Still need to edit GRUB and possibly remove Devuan's wallpapers.
Need to edit the "Debian Respin" label.

I wanted to add MakeHuman, but it created a 4.1GB squashfs. I may be able to get
the needed room by removing 'hydrogen-drumkits-effects' package.

A created ISO finally installed successfully.

Note to self: Don't ever bother installing 'hydrogen-drumkits-effects.' All it
              does is add belofilms.com-AC-Guitar-Strums (flac), which I and 
              probably most don't ever use. It's removal frees-up about 143MB
              of space.
