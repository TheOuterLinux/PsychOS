#####################################################################################
# PsychOS 08/19/2016 - PsychOS 2.5.6 - xdg, wallpapers, srm, and customizing panels #
#####################################################################################

* I tried pasting over the contents of xdg for XFCE; the first build of 
  that ended in failure.
  
* Desktop images added to XFCE
    - Still need to add wallpapers to GNOME and IceWM
    
* Might add Grive to the menu
    - The Ubuntu versions of this process is much easier because a gui
      already exists.
        [] I may just use alien to convert the deb packages to rpm.
        
* w3m menu item needs to be fixed; wrong name; "w3m.desktop" appears in 
  the menu
  
* Installed srm for secure file removal
    - On Mac, srm writes random garbage over the file(s) 35 times before
      completely removing by default; far exceeds military grade removal.
    - Incredibly slim chance of getting it back.
    - Great for sensitive data
    - sudo srm -vr [file or folder]
        [] Uses -vr flag instead of rm -fr or -rf flags; no idea why
        
* Placed the wallpapers for XFCE in the wrong folder

* Most of the default XFCE setup wanted for all users works except the 
  bottom (2nd) panel.
    - Only the "Show Desktop" and "Directory Menu" on either end of the 
      bottom panel show up; the rest are just blank launchers.
    - However, at least the keyboard shortcuts and power saving settings
      transferred successfully
      
* For GNOME wallpapers, I'll have to edit 
  /usr/share/gnome-backgroundproperties/gnome-default.xml
  
* Forgot to add keyboard shortcuts to launch the terminal.

* Idea: Add secure empty trash via srm; may not be possible, but look 
  into it anyway.
  
* Need to customize IceWM menu to make it more like XFCE's default 
  applications menu
