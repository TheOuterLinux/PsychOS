##############################################################################
# PsychOS 09/21/2016 - PsychOS 2.5.7 - A review of things to still look into #
##############################################################################

* 09/07/2016
    - GIMP & Blender addons by default
    - There's an addon with MakeHuman-like functionality that could be
      used to save space
        [] Update [09/26/2016]: Removed Hydrogen drums to make room for 
           the actual MakeHuman application
    - Apple earphone compatibility
    - Add Kodi repos by default
    
* 09/08/2016
    - Drop Chromium if Firefox get Widevine support by default
        [] Update [09/26/2016]: Firefox 49 for Linux will play Netflix 
           but the user agent has to be changed to
           reflect the latest version of Chrome.
        [] Chromium and the Widevine package will be kept on TheOuterLinux 
           repos just in case.
    - LibreOffice templates packages added an option to right-click
      menu inside "Create Document."
        [] Update [09/26/2016]: Now it doesn't work for some reason. It 
           could be a LibreOffice version conflict, so it will be kept 
           just in case there's an update that fixes it.

* 09/11/2016
    - May want to think about disc partitioning to see if possible
    
* 09/14/2016
    - Live Installer keeps picking Afrikaans as its default language
    
* 09/19/2016
    - Wanted to install terminator as a way to have split-view 
      command-line, but it is only useful when X is running.
      
* 09/20/2016
    - cdw is missing icon in menu
    - alpine mail app is in wrong category; may remove from menu
        [] Update [09/26/2016]: alpine was removed from the menu
    - Copy power management settings from home folder
        [] Update [09/26/2016]: I did this and now suspend on lid shut 
           by default wasn't there like it was before.
