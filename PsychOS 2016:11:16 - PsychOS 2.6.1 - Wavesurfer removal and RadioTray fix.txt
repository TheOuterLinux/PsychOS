#####################################################################
# 2016/11/16 - PsychOS 2.6.1 - Wavesurfer removal and RadioTray fix #
#####################################################################

*Moved wavesurfer binary from /bin/ to /usr/bin/

*Need to see if I can replace the default kernel with the PAE version just with the "Software" tab on SuseStudio. Then, check and see if the disc is required on the PC/BIOS computer.

*Need to install scons, libSDL-devel, intltool

*Can edit RadioTray bookmarks.xml inside /usr/share/radiotray/
