##############################################################
# 2017/04/26 - PsychOS 2.7.1 - Kernel updates causing issues #
##############################################################

*Since kernel 4.10.10, PsychOS has been running into weird issues such as login taking forever and suspend not really working; the screen blanks, but the fans still run and the indicator light is not blinking like it should; this is on a MacBook 2008 pre October 4,1 model.