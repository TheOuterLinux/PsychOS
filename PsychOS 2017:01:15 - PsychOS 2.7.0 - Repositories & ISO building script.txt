###################################################################
# 2017/01/15 - PsychOS 2.7.0 - Repositories & ISO building script #
###################################################################
*Finally added  fontforge after adding a Fedora 25 repository
*Accidentally spent several hours creating a 64-bit version of PsychOS; however, the repository also works for 32-bit. Thank God...
*Found a very interesting script:
	- mkisofs -R -D -V "Name of ISO" -o /tmp/fdbasecd-remastered.iso -b ./isolinux/isolinux.bin -c ./isolinux/boot.cat -no-emul-boot -boot-load-size 4 -boot-info-table ./
