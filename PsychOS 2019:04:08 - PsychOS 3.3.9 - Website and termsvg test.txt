#########################################################
# 2019/04/08 - PsychOS 3.3.9 - Website and termsvg test #
#########################################################

I've been working on the GitLab .io site for "TheOuterLinux" non-stop for
the last few days and I'm nowhere near done. Rehunting down download links
for recommended DOS software has been very challenging.

Did a quick test of 'termtosvg' with CLIMax and it went really well. Now,
it would be great to have an animated SVG to GIF converter. Most modern
web browsers will handle it just fine, but I would like to have something
for older ones too. I guess I could just use Peek to record the animated
SVG as an animated GIF.

I did a test of 'termtosvg' while in X on main computer and then used
Peek to record as an animated GIF. But while watching, it seems as though
termtosvg rendered ncurses as ascii. It's possible that is because of X
environment instead of TTY. However, maybe PsychOS just kicks ass.
