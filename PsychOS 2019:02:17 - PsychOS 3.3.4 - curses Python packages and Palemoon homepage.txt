#############################################################################
# 2019/02/17 - PsychOS 3.3.4 - curses Python packages and Palemoon homepage #
#############################################################################

Found a cool Python package called 'xdgmenu' that is basically an applications 
menu but for the command-line; however, it doesn't know the difference between 
GUI and TUI programs.

'chronos' - ncurses stopwatch/timer

I can use 'sudo pip install comp' for a curses frontend to 'mpv'.

'treepick' is a tree-style file-picker

'pulsemixer' - curses PulseAudio mixer

'wikicurses' - curses interface for Wikipedia

'igrepper' - interactive grepping tool

I guess I would add 'livestreamer-curses' for live stream management

I've got most of the 'Palemoon' homepage finished. It's using mostly plain HTML
except for the RSS section, which uses JavaScript from https://feed2js.org and when
there isn't any JS support, there are still clickable links that result in opening
the feeds with 'Liferea'. The problem now is to get a .io site up and running for
it. I may call the repo "PsychOSWeb" and name the homepage as "homepage.html" but
have the "index.html" be to sort of showcase PsychOS. In otherwords, homepage.html
will just be sort of off to the side with nothing linking to it except 'Palemoon.'

Added Super+G for "ClipColor" script to add a hexcode color picked copied to the
clipboard.

For some reason, I couldn't think of the open-source EMR I had in PsychOS 2.x, so
I ran 'apt search medical' and found it, as well as remembering to add 'SigViewer'
(EEG, EMG, etc.) and 'xmedcon', a medical image conversion tool.

There is a framebuffer version of 'netsurf' I would like to get working, but I
cannot seem to.

I organized 'pyradio's' stations a little better and went ahead and added a
'radiotray' folder with "bookmarks.xml" and so forth just in case someone does
install 'radiotray.'  This is because many of the default stations don't work
anymore. I would include it by default, but it is buggy and 'pyradio' is much
better, except it lacks asx support.

I added a Linux-themed version of 'dopewars' by editing the .desktop file and 
".dopewars" file.

I was going to add all of 'vice's' (Commodore emulator) to the games category but
rather than take-up room, I only moved C64 to it.