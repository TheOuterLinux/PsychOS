#####################################################
# 2017/02/07 - PsychOS 2.7.0 - IceWM & USB mounting #
#####################################################

*In IceWM, Reboot and Shutdown do not work from the menu.

*If USB cannot mount for some reason, create a folder inside /run/[username]

*nm-applet and blueman-applet run in IceWM just fine at first, but eventually run seperately in a single tiny window; it's an on/off "iffy" kind of thing.
