###########################################################################
# 2017/04/30 - PsychOS 2.7.1 - Internet browsing and Bluetooth in climenu #
###########################################################################

*I thought it would be a good idea to install links as a w3m substitute, but even after adding the official Tumbleweed networks repository, there are dependencies missing.

*One of the next items I need to add is file searching to climenu

*Bluetooth controls needed in climenu
	*I may be able to use hcitool for this
		*sudo hciconfig hci0 up/down
		*hcitool dev to get devices
	*hciconfig -a hci0 displays devices info
	*hciconfig hci0 features displays features for device

*w3m "Do you need cookies?" display box needed.
	*w3m -cookie [website]
		*saves cookies to ./w3m/cookie