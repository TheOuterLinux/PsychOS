###################################################################################
# 2019/02/25 - PsychOS 3.3.6 - Polkit errors and Blender OpenGL issue work-around #
###################################################################################

I'm having 'polkit'-related errors on one of my computer and I think a 'dist-upgrade'
caused it.

'Blender' will not run on one of my computers because for some reason it doesn't
think it can handle OpenGL 2.1, which it can so, I can get it working with:

    LIBGL_ALWAYS_SOFTWARE=1 blender

I need to learn more about how IceWM's menu item placement works.

Download copied links to an opened folder Thunar custom action:

    wget -c "$(xsel -clipboard -output)"