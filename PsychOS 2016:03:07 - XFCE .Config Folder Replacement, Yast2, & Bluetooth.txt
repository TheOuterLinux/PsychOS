###########################################################################
# PsychOS 03/07/2016 - XFCE .Config Folder Replacement, Yast2, & Bluetooth #
###########################################################################

* Forgot to record uptime and RAM usage for the memory leak test (see PsychOS
  03/06/2016). However, after 1.5 hours, RAM usage went down.

* Borrowing XFCE config folder was stupid (hidden in /home).

* After adding panel items, startup RAM is 150MB

* Replacing the folder did add keyboard shortcuts and removed the XFCE 
  splash screen, but that's about it.

* GUFW (popular Debian/Ubuntu firewall) only works when launched from the 
  terminal.

* Needs installed:
    - Anki for flash cards
    - SigViewer to view EEG data
    - Asunder CD Ripper
    - Cameramonitor icon appears in panel if webcam is in use
    - Labyrinth Mind Mapping
    - Check (PsychOS 03/06/2016 for needed Tex, Latex, and Lyx packages

** Turns out the use of Bleachbit had nothing to do with the missing GDM; 
   if it did, the system would have probably broke eventually anyhow.

* USB works fine
* WiFi works fine
* Internet works

* The YAST2 Software Management program seems to be the best for updates;
  sudo zypper update works too.

* I think some Gstreamer packages may have made it into PsychOS somehow.

* Lid shutting for suspend (sleep) works

** Next build is going to have GDM removed and replaced with LightDM. 
   Need to make sure to clone the application first.

* Update [2016:03:20]: PsychOS's next build did not have LightDM
  installed.

* PsychOS currently has 10 repositories

* Building PsychOS 2.0.1
    - Tried removing the Gstreamer packages, but I can't figure out which 
      programs are using them. Besides, last time I checked, they didn't 
      work.

* The applications and packages listed under "Needs installed" will not 
  be installed unless built from source and an ISO is compiled later.

* The PsychOS 2.0.1 build is taking forever to make an ISO. I added a
  "TeX" repo to PsychOS and a lot of updated versions of TeX packages are 
  being installed/added.

* Left a test file in the home directory to check for /home preservation 
  during a reinstall.

* Thought I'd forgot to install Bluetooth stuff, but it turns out that 
  GNOME was hogging it all. So, I installed Blueman (PsychOS 2.1.0).
