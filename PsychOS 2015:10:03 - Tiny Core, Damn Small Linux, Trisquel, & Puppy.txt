######################################################################
# PsychOS 10/03/2015 - Tiny Core, Damn Small Linux, Trisquel, & Puppy #
######################################################################

* I've tried Tiny Core, Damn Small Linux, and Trisquel; none of these distros will make the cut. I've decided to go with Puppy Linux again.

* Tried Quirky April Puppy Linux. I'm having trouble with the WiFi and creating a frugal  save of the same kind.It claims to be created for a hard install. To save my progress, I need 4 GB of RAM and an optical drive. I may have to go back to a previous  Puppy (v6).
