################################################################################################
# PsychOS 09/11/2016 - PsychOS 2.5.7 - Looking into assistance tools for eye and head movement #
################################################################################################

* Installed OpenJDK 1.6 for legacy software support
    - Wanted to install osptracker, but it's a .run file for Linux users
        [] I'll have to install manually
        
* Found a lot of eye-tracking software:
    - PyGaze
    - OpenGazer (use the GitHub version)
    - TrackEye; may be Windows only
    
* MouseTrap is for mouse movement but uses your head and a webcam to move
  the mouse around

* Need to install:
    - python-imaging
    - python-pygame
    - pyglet
    - wxPython
    - psychopy
    
* May want to see about partitioning a disc; first partition contains 
  PsychOS and the second contains documentation.
