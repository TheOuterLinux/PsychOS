#########################################################
# 2019/05/01 - PsychOS 3.4.5 - BristolCLI script doubts #
#########################################################

Played around with BristolCLI script and now I'm not so sure if I'll
include it with PsychOS or not.

Nevermind, I'll include it with a menu item to sort of say it's still
a work-in-progress.
