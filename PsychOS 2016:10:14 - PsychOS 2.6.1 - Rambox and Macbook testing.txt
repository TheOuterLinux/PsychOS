###########################################################
# 2016/10/14 - PsychOS 2.6.1 - Rambox and Macbook testing #
###########################################################

*I'm getting rid of Rambox but keeping it in the repo just in case. It's not worth the space and noticed a potential security risk while browsing the hidden files in my /home folder. It's basically a browser that keeps cookies, gzip files full of who knows what, and images from activated services. Images have been known to carry tracking info. It's kind of buggy anyway. I may replace it with Franz.

*I may start testing PsychOS builds on my 2008 Macbook, or at least "upgrade" it with each official release.

*Just tried to install PsychOS 2.6.1 on my 2008 and it failed. I had a 2MB partition at the very beginning that may of been needed. GParted said it was a Linux partition, but it had a black color.

*I found a fix for the 30 second white screen of death on my Macbook. I have to boot up the Mac OSX disc and open a terminal and enter:
	$ bless -device /dev/disk0s1 -setBoot -legacy
