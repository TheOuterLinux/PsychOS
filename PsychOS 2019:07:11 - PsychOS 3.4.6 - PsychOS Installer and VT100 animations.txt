#######################################################################
# 2019/07/11 - PsychOS 3.4.6 - PsychOS Installer and VT100 animations #
#######################################################################

I've been working on the PsychOS Installer some more.

I added some more file formats to the Release Notes list and added a
Thunar Custom Action for playing VT100 animation files (*.vt).

I just realized that PsychOS includes tools made by MX Linux for things
such as changing user settings, keyboard, and locales. This means that
I can have the PsychOS Installer default to those tools and then use
'dpkg-reconfigure' if missing. However, this also means providing two
video URL's for the help buttons.
