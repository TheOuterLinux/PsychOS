####################################
# 2017/06/13 - PsychOS 2.7.1 - vim #
####################################

*I hate vim; it's got to be removed from anything I make. I am sick and tired of command line programs defaulting to it when config files need to be edited. Nano is a thousand times easier to use.