##################################################################
# PsychOS 09/28/2016 - PsychOS 2.6.0 - Minitube and bug tracking #
##################################################################

* Install Minitube to make YouTube watching much much easier for both the user
and on RAM & CPU

* Looking into bug tracking software. I'll probably use Mantis since they have
more free stuff, including mobile apps.
