##########################################################################
# 2019/07/12 - PsychOS 3.4.6 - PsychOS Installer, testing Live, and LMMS #
##########################################################################

Instead of using other people's videos for the PsychOS Installer, I've
decided to just create my own but will still have the option to change
the video URL whenever I need to.

During bootup of PsychOS live, I noticed that the screen hangs in a
console just long enough that someone may think they need to login that
way as opposed to just waiting long enough for the desktop to load. So,
I may need to add a message about that to the "/etc/issue" file.

Another thing I noticed, because I have an external monitor connected, is
people may not know about the SUPER+p keyboard shortcut for quickly
changing displays. So, it may be a good idea to add a Displays button to
a Welcome menu.

I've been having the idea of a repository for my custom scripts and
settings for a while and having programs like 'pyradio' lose stations is
just helping push more towards the idea.

Played around with LMMS while live and I think I might add a 
"TheOuterLinux Favorites" to the My Presets section, but this will be 
done in "/usr/share/lmms" after moving from the /home folder because it
would be easier that way.
