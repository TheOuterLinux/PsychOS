##################################################################################################
# PsychOS 09/07/2016 - PsychOS 2.5.7 - Medical, research-related software, and a few other ideas #
##################################################################################################

* May need to start looking into medical applications for PsychOS

* Need to find GIMP and Blender addon locations

* MakeHuman for research?
  - Had a conversation at a SEPA (South-Eastern Psychology
    Association) conference for an idea to use MakeHuman for
    research involving self-image and avatars.
  - There's an addon for Blender that will do a similar job but save
    some space.
    
* Look into adding an Apple earphone-like (three ring) functionality

* Need to test more Bluetooth related stuff; only tested headphones

* Not sure if WACOM tablet has been tested or not

* WINE is installed, but not adequately tested.

* PsychOS is built upon an awesome platform, but curiosity wants me to 
  try to make a more portable version via Puppy Linux and see if a 
  Raspberry Pi version can be made via PiBakery.
  
* A WYSIWYG editor for web/ html development?
    - Just a thought; probably not gonna happen.
    
* There's an open source alternative to Adobe Publisher called Scribus I
  may install.
  
* Need to figure out a way to add repositories to Kodi by default.

* Would like to find some EEG analysis software; it doesn't even have to 
  be in real time.
    - Tried to compile some EEG software from source code; not
      working.
    - There are open source software options out there, but most are for
      Windows or require MATLAB.
      
* I may just go ahead and install RetroArch or an alternative.
    - Update [09/09/2016]: compiling the super version from source code
      takes waaaaay too long and when it is finished, it is way too buggy
      to be usable.
      
* May try to figure out how to inject metadata into .desktop files to make
  searching for applications via the Whiskermenu easier.
