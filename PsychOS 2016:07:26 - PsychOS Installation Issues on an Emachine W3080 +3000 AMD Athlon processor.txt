##############################################################################
# PsychOS 07/26/2016 - PsychOS Installation Issues on an Emachine W3080 3000+ #
# AMD Athlon processor                                                       #
##############################################################################

* PsychOS was tested on an Emachines W3080 with a 3000+ AMD Athlon processor
  and some kind of S.M.A.R.T. hard drive checking technology. There was 
  issues installing PsychOS. It is as if GRUB wasn't installed properly. 

* The S.M.A.R.T.  checking can't recognize EXT filesystem formats higher 
  than EXT2. It skips over the Linux-Swap partition as unrecognizable. 
  
* The hd0, 3 partition doesn't have any messages associated with it. 
  Nothing can be typed, but the system will reboot via CTRL+ALT+DEL.

* The hard drive may need to be wiped and have only a single partition 
  and pick one of the default options. This will be done graphically 
  rather than initiating YaST through a terminal; if this works, a retry 
  will be done with multiple partitions as /boot (EXT2), / (EXT4), Linux-swap, 
  and /home (EXT4).
